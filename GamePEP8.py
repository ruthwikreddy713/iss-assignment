#!/usr/bin/python
# -*- coding: utf-8 -*-
import pygame
import random
import math
import config
import time
pygame.init()
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption('CROCS')
Obimg = []
H1X = []
H2X = []
H2Y = []
H1Y = []

# Player check String to see which Player is active

player_check = 'Player1'

# iterator used for loops

iterator = 0

# LEvels are used Just for increasing the speed of obstacles

Player1_level = 0
Player2_level = 0

# LIST OF STATIONARY OBJECTS TYPE1 JUST MEANS THE LOOP i USED H2

for iterator in range(4):
    Obimg.append(pygame.image.load('stat.png'))
    H1X.append(100 + 120 * iterator)
    H1Y.append(460 - 120 * iterator)
    H2X.append(700 - 120 * iterator)
    H2Y.append(460 - 120 * iterator)


# PRINTING THE OBSATACLE FUNCTION

def ObsTy1(H1X, H1Y, i):
    screen.blit(Obimg[i], (H1X, H1Y))


# PLAYER 1 INFO

Player1img = pygame.image.load('superhero.png')
P1X = 300
P1Y = 570
P1_start_Time = 0
P1_changeX = 0
P1_changeY = 0


# PLAYER 2 INFO
# def ShowTime(time,x,y):

def Player1(P1X, P1Y):
    screen.blit(Player1img, (P1X, P1Y))


Player2img = pygame.image.load('superhero.png')
P2X = 300
P2Y = -10
P2_changeX = 0
P2_changeY = 0
clock = pygame.time.Clock()


def Player2(P2X, P2Y):
    screen.blit(Player2img, (P2X, P2Y))


# MovingObject
# WHEN PLAYER SUCCESSFULLY COMPLETES ROUND

def Success():
    SoverOt = config.over_font.render(config.SuccessM, True,
            config.GREEN)
    screen.fill(config.WHITE)
    screen.blit(SoverOt, (200, 200))
    pygame.display.update()
    pygame.time.wait(2000)


# config.score_font = pygame.font.Font('freesansbold.ttf', 15)
# SHOWUNG SCORE FUNCTION

def Showscore1(score):
    score_val = config.score_font.render('SCORE :  ' + str(score),
            True, (255, 255, 255))
    screen.blit(score_val, (10, 8))


def Showscore2(score):
    score_val = config.score_font.render('SCORE : ' + str(score), True,
            (255, 255, 255))
    screen.blit(score_val, (700, 8))


# DISPLAYING TIME

def Times(x, time, y):
    time_val = config.score_font.render('Time  :  ' + str(time), True,
            (255, 255, 255))
    screen.blit(time_val, (x, y))


# PRINTING WINNERS

def WinnerP1():
    GoverOt = config.over_font.render(config.P1WINM, True, config.GREEN)
    screen.fill(config.WHITE)
    screen.blit(GoverOt, (200, 200))
    pygame.display.update()
    pygame.time.wait(2000)


def WinnerP2():
    GoverOt = config.over_font.render(config.P2WINM, True, config.GREEN)
    screen.fill(config.WHITE)
    screen.blit(GoverOt, (200, 200))
    pygame.display.update()
    pygame.time.wait(2000)


iterator = 0
MovingOImg = []
MovingOX = []
MovingOY = []
MovingOX_changeX = []
MovingOY_changeY = []
for iterator in range(5):
    MovingOImg.append(pygame.image.load('croc.png'))
    MovingOX.append(random.randint(0, 800))
    MovingOY.append(random.randint(490 - iterator * 120, 510 - iterator
                    * 120))
    MovingOX_changeX.append(0.1)
    MovingOY_changeY.append(0)
MovingOY[4] = 40
MovingOX[4] = 20


# PRINTING MOVING 0

def MovingO(x, y, i):
    screen.blit(MovingOImg[i], (x, y))


# WELCOME

def Welcome():
    WoverOt = config.over_font.render('Welcome', True, config.GREEN)
    screen.blit(WoverOt, (200, 200))


# just A DUCK TO DISTRACT

MovingO2Img = pygame.image.load('duck.png')
MovingO2X = random.randint(0, 800)
MovingO2Y = random.randint(340, 380)
MovingO2X_changeX = 0.1
MovingO2Y_changeY = 0


# DUCK MOVING

def MovingO2(x, y):
    screen.blit(MovingO2Img, (x, y))


P2_start_Time = 0


# COLLISION WITH STATIC

def CollisionS(
    P1X,
    P1Y,
    O1X,
    O1Y,
    ):

    distnce = math.sqrt(math.pow(P1X - O1X, 2) + math.pow(P1Y - O1Y, 2))
    if distnce < 50:
        return True
    else:
        return False


# COLLISION MOVING

def CollisioinM(
    P1X,
    P1Y,
    O1X,
    O1Y,
    ):

    distnce = math.sqrt(math.pow(P1X - O1X, 2) + math.pow(P1Y - O1Y, 2))
    if distnce < 50:
        return True
    else:
        return False


def Round_Info(round):
    RoverOt = config.score_font.render('ROUND : ' + str(round), True,
            (0, 255, 0))
    screen.blit(RoverOt, (400, 10))


iterator = 0
List_Stat_1 = [False for iterator in range(4)]
iterator = 0
List_Mov = [False for iterator in range(5)]
iterator = 0


# config.over_font = pygame.font.Font('freesansbold.ttf', 64)

def GameoverM():
    GoverOt = config.over_font.render(config.GameoverM, True,
            config.GREEN)
    screen.blit(GoverOt, (200, 200))


score1 = 0
score2 = 0
checkP1 = 0
checkP2 = 0
rounds = 0
running = 0

 # variable TO make the screen running
# WELCOME DUDE

screen.fill(config.RED)
Welcome()
pygame.display.update()
time.sleep(2)

# MAIN LOOOOOOOO(iNFINITE)LOOP

while rounds < 8 and running == 0:
    screen.fill(config.WATER)
    pygame.draw.line(screen, config.SOIL, (0, 600), (800, 600), 80)
    pygame.draw.line(screen, config.SOIL, (0, 480), (800, 480), 40)
    pygame.draw.line(screen, config.SOIL, (0, 360), (800, 360), 40)
    pygame.draw.line(screen, config.SOIL, (0, 240), (800, 240), 40)
    pygame.draw.line(screen, config.SOIL, (0, 120), (800, 120), 40)
    pygame.draw.line(screen, config.SOIL, (0, 0), (800, 0), 80)
    start = config.score_font.render('START P1', True, config.WHITE)
    screen.blit(start, (350, 580))
    end = config.score_font.render('END P2', True, config.WHITE)
    screen.blit(end, (350, 20))
    Player2(P2X, P2Y)
    iterator = 0
    ObsTy1(H1X[0], H1Y[0], 0)
    ObsTy1(H1X[1], H1Y[1], 1)
    ObsTy1(H1X[2], H1Y[2], 2)
    ObsTy1(H1X[3], H1Y[3], 3)
    ObsTy1(H2X[0], H2Y[0], 0)
    ObsTy1(H2X[1], H2Y[1], 1)
    ObsTy1(H2X[2], H2Y[2], 2)
    ObsTy1(H2X[3], H2Y[3], 3)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = -1
    if player_check == 'Player1':
        P1_start_Time += 1
        time1 = P1_start_Time // 500
        iterator = 0
        for iterator in range(5):
            MovingOX[iterator] += MovingOX_changeX[iterator]  # % 800
            MovingOY[iterator] += MovingOY_changeY[iterator]  # % 600
            if MovingOX[iterator] <= 0:
                MovingOX_changeX[iterator] = 0.1 + 0.1 * Player1_level
            elif MovingOX[iterator] >= 750:
                MovingOX_changeX[iterator] = -0.1 - 0.1 * Player1_level
            MovingO(MovingOX[iterator], MovingOY[iterator], iterator)
        MovingO2X += MovingO2X_changeX  # %
        MovingO2Y += MovingO2Y_changeY  # % 600
        if MovingO2X <= 0:
            MovingO2X_changeX = 0.1 + 0.1 * Player1_level
        elif MovingO2X >= 750:
            MovingO2X_changeX = -0.1 - 0.1 * Player1_level
        MovingO2(MovingO2X, MovingO2Y)
        P2X = 300
        P2Y = -10
        Player2(P2X, P2Y)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                P1_changeX = -0.1
                P1X += P1_changeX
                P1_changeX = 0
            if event.key == pygame.K_RIGHT:
                P1_changeX = 0.1
                P1X += P1_changeX
                P1_changeX = 0
            if event.key == pygame.K_UP:
                P1_changeY = -0.1
                P1Y += P1_changeY
                P1_changeY = 0
            if event.key == pygame.K_DOWN:
                P1_changeY = 0.1
                P1Y += P1_changeY
                P1_changeY = 0
        if P1X <= 0:
            P1X = 0
        elif P1X >= 780:
            P1X = 780
        elif P1Y < 5:
            rounds += 1
            Success()
            Player1_level += 1
            player_check = 'Player2'
            P1Y = 5
            iterator = 0
            temp = rounds // 2 + 1
            screen.fill(config.WHITE)
            Round_Info(temp)
            pygame.time.wait(1000)
            for iterator in range(4):
                List_Stat_1[iterator] = False
            for iterator in range(5):
                MovingOX_changeX[iterator] = 0.1 + 0.1 * Player1_level
                MovingOX[iterator] += MovingOX_changeX[iterator]
                List_Mov[iterator] = False
        elif P1Y >= 580:
            P1Y = 580
        Player1(P1X, P1Y)

        iterator = 0
        for iterator in range(4):
            if CollisionS(P1X, P1Y, H1X[iterator], H1Y[iterator]):
                screen.fill(config.WHITE)
                GameoverM()
                pygame.display.update()
                time.sleep(2)
                P1X = 300
                P1Y = 570
                checkP1 += score1
                score1 = 0
                rounds += 1
                temp = rounds // 2 + 1
                screen.fill(config.WHITE)
                Player1_level += 1
                P1_changeX = 0
                MovingOX.append(random.randint(0, 800))
                MovingOY.append(random.randint(480 - iterator * 120,
                                500 - iterator * 120))
                MovingOX[0] = 50
                MovingOY[0] = 510
                MovingOX[4] = 20
                MovingOY[4] = 40
                P1_changeY = 0
                for iterator in range(5):
                    MovingOX_changeX[iterator] = 0.1 + 0.1 \
                        * Player1_level
                    MovingOX[iterator] += MovingOX_changeX[iterator]
                Player1(P1X, P1Y)
                player_check = 'Player2'
            elif CollisionS(P1X, P1Y, H2X[iterator], H2Y[iterator]):
                screen.fill(config.WHITE)
                GameoverM()
                pygame.display.update()
                time.sleep(2)
                Player1_level += 1
                P1X = 300
                P1Y = 570
                checkP1 += score1
                for iterator in range(5):
                    MovingOX_changeX[iterator] = 0.1 + 0.1 \
                        * Player1_level
                    MovingOX[iterator] += MovingOX_changeX[iterator]
                score1 = 0
                rounds += 1
                temp = rounds // 2 + 1
                screen.fill(config.WHITE)
                Round_Info(temp)
                pygame.display.update()
                pygame.time.wait(1000)
                MovingOX.append(random.randint(0, 800))
                MovingOY.append(random.randint(490 - iterator * 120,
                                510 - iterator * 120))
                MovingOX[0] = 50
                MovingOY[0] = 510
                MovingOX[4] = 20
                MovingOY[4] = 40
                P1_changeX = 0
                P1_changeY = 0
                player_check = 'Player2'
                P1_GameS = 'Over'

        iterator = 0
        for iterator in range(5):
            if CollisioinM(P1X, P1Y, MovingOX[iterator],
                           MovingOY[iterator]):
                screen.fill(config.WHITE)
                GameoverM()
                pygame.time.wait(500)
                player_check = 'Player2'
                pygame.display.update()
                time.sleep(2)
                P1X = 300
                P1Y = 570
                Player1_level += 1
                for iterator in range(5):
                    MovingOX_changeX[iterator] = 0.1 + 0.1 \
                        * Player1_level
                    MovingOX[iterator] += MovingOX_changeX[iterator]
                rounds += 1
                temp = rounds // 2 + 1
                screen.fill(config.WHITE)
                checkP1 += score1

                # print(time)

                score1 = 0
                player_check = 'Player2'
                P1_GameS = 'Over'
                MovingOX.append(random.randint(0, 800))
                MovingOY.append(random.randint(490 - iterator * 120,
                                510 - iterator * 120))
                MovingOX[0] = 50
                MovingOY[0] = 510
                MovingOX[4] = 20
                MovingOY[4] = 40
                P1_changeX = 0
                P1_changeY = 0
            Times(10, time1, 20)
        score1 = 0
        iterator = 0
        for iterator in range(4):
            if List_Stat_1[iterator] == False:
                if P1Y < H1Y[iterator]:
                    score1 += 10
                    List_Stat_1[iterator] = True
        iterator = 0
        for iterator in range(5):
            if List_Mov[iterator] == False:
                if P1Y < MovingOY[iterator]:
                    score1 += 10
                    List_Mov[iterator] = True

    # print(P1_start_Time)

    Showscore1(score1)
    iterator = 0
    for iterator in range(4):
        List_Stat_1[iterator] = False
    iterator = 0
    for iterator in range(5):
        List_Mov[iterator] = False
    iterator = 0
    if player_check == 'Player2':
        score2 = 0
        P2_start_Time += 1
        time2 = P2_start_Time // 500
        iterator = 0
        for iterator in range(5):
            MovingOX[iterator] += MovingOX_changeX[iterator]  # % 800
            MovingOY[iterator] += MovingOY_changeY[iterator]  # % 600
            if MovingOX[iterator] <= 0:
                MovingOX_changeX[iterator] = 0.1 + 0.1 * Player1_level
            elif MovingOX[iterator] >= 750:
                MovingOX_changeX[iterator] = -0.1 - 0.1 * Player1_level
            MovingO(MovingOX[iterator], MovingOY[iterator], iterator)
        MovingO2X += MovingO2X_changeX  # %
        MovingO2Y += MovingO2Y_changeY  # % 600
        if MovingO2X <= 0:
            MovingO2X_changeX = 0.1 + 0.1 * Player1_level
        elif MovingO2X >= 750:
            MovingO2X_changeX = -0.1 - 0.1 * Player1_level
        MovingO2(MovingO2X, MovingO2Y)
        P1X = 300
        P1Y = 580
        Player1(P1X, P1Y)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                P2_changeX = -0.1
                P2X += P2_changeX
                P2_changeX = 0
            if event.key == pygame.K_d:
                P2_changeX = 0.1
                P2X += P2_changeX
                P2_changeX = 0
            if event.key == pygame.K_w:
                P2_changeY = -0.1
                P2Y += P2_changeY
                P2_changeY = 0
            if event.key == pygame.K_s:
                P2_changeY = 0.1
                P2Y += P2_changeY
                P2_changeY = 0
        if P2X <= 0:
            P2X = 0
        elif P2X >= 780:
            P2X = 780
        elif P2Y < -10:
            P2Y = -10
        elif P2Y >= 580:
            rounds += 1
            Success()
            Player2_level += 1
            player_check = 'Player1'
            P1Y = 5
            iterator = 0
            temp = rounds // 2 + 1
            for iterator in range(4):
                List_Stat_1[iterator] = False
            for iterator in range(5):
                MovingOX_changeX[iterator] = 0.1 + 0.1 * Player1_level
                MovingOX[iterator] += MovingOX_changeX[iterator]
                List_Mov[iterator] = False
        Player2(P2X, P2Y)

        iterator = 0
        for iterator in range(4):
            if CollisionS(P2X, P2Y, H1X[iterator], H1Y[iterator]):
                screen.fill(config.WHITE)
                GameoverM()
                pygame.display.update()
                time.sleep(2)
                P2X = 300
                P2Y = -10
                checkP2 += score2
                score2 = 0
                rounds += 1
                temp = rounds // 2 + 1

                # screen.fill(config.WHITE)

                Player2_level += 1
                P2_changeX = 0
                MovingOX.append(random.randint(0, 800))
                MovingOY.append(random.randint(480 - iterator * 120,
                                500 - iterator * 120))
                MovingOX[0] = 50
                MovingOY[0] = 510
                MovingOX[4] = 20
                MovingOY[4] = 40
                P2_changeY = 0
                for iterator in range(5):
                    MovingOX_changeX[iterator] = 0.1 + 0.1 \
                        * Player2_level
                    MovingOX[iterator] += MovingOX_changeX[iterator]
                Player2(P2X, P2Y)
                player_check = 'Player1'
            elif CollisionS(P2X, P2Y, H2X[iterator], H2Y[iterator]):
                screen.fill(config.WHITE)
                GameoverM()
                pygame.display.update()
                time.sleep(2)
                Player2_level += 1
                P2X = 300
                P2Y = -10
                checkP2 += score2
                for iterator in range(5):
                    MovingOX_changeX[iterator] = 0.1 + 0.1 \
                        * Player2_level
                    MovingOX[iterator] += MovingOX_changeX[iterator]
                score2 = 0
                rounds += 1
                temp = rounds // 2 + 1
                screen.fill(config.WHITE)
                Round_Info(temp)
                pygame.display.update()
                pygame.time.wait(1000)
                MovingOX.append(random.randint(0, 800))
                MovingOY.append(random.randint(480 - iterator * 120,
                                500 - iterator * 120))
                MovingOX[0] = 50
                MovingOY[0] = 510
                MovingOX[4] = 20
                MovingOY[4] = 40
                P2_changeX = 0
                P2_changeY = 0
                player_check = 'Player1'
                P1_GameS = 'Over'

        iterator = 0
        for iterator in range(5):
            if CollisioinM(P2X, P2Y, MovingOX[iterator],
                           MovingOY[iterator]):
                screen.fill(config.WHITE)
                GameoverM()
                pygame.time.wait(500)
                player_check = 'Player1'
                pygame.display.update()
                time.sleep(2)
                P2X = 300
                P2Y = -10
                Player2_level += 1
                for iterator in range(5):
                    MovingOX_changeX[iterator] = 0.1 + 0.1 \
                        * Player2_level
                    MovingOX[iterator] += MovingOX_changeX[iterator]
                rounds += 1
                temp = rounds // 2 + 1
                screen.fill(config.WHITE)
                checkP2 += score2

                # print(time)

                score2 = 0
                player_check = 'Player1'
                P1_GameS = 'Over'
                MovingOX.append(random.randint(0, 800))
                MovingOY.append(random.randint(480 - iterator * 120,
                                500 - iterator * 120))
                MovingOX[0] = 50
                MovingOY[0] = 510
                MovingOX[4] = 20
                MovingOY[4] = 40
                P2_changeX = 0
                P2_changeY = 0
            Times(700, time2, 20)
        iterator = 0
        score2 = 0
        iterator = 0
        for iterator in range(4):
            if List_Stat_1[iterator] == False:
                if P2Y > H1Y[iterator]:

                    score2 += 10
                    List_Stat_1[iterator] = True
        iterator = 0
        for iterator in range(5):
            if List_Mov[iterator] == False:
                if P2Y > MovingOY[iterator]:
                    score2 += 10
                    List_Mov[iterator] = True

    # print(P1_start_Time)

     #   print score2

        Showscore2(score2)
    Round_Info(rounds // 2 + 1)
    if rounds == 8:
        if checkP1 > checkP2:
            WinnerP1()
        elif checkP1 < checkP2:
            WinnerP2()
        else:
            if time1 > time2:
                WinnerP2()
            else:
                WinnerP1()
    pygame.display.update()
