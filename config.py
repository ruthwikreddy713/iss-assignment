#!/usr/bin/python
# -*- coding: utf-8 -*-
import pygame
import time
pygame.init()

# COLOR

WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
WATER = (173, 216, 255)
SOIL = (203, 65, 84)

# Font

pygame.font.init()
score_font = pygame.font.Font('freesansbold.ttf', 15)
over_font = pygame.font.Font('freesansbold.ttf', 64)

 # MESSAGE

GameoverM = 'GAME OVER'
SuccessM = 'SUCCESS'
P1WINM = 'P1 IS WINNER'
P2WINM = 'P2 IS WINNER'
